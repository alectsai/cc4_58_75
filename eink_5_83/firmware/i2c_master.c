//----------------------------------------------------------------------------
// i2c_master.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void i2c_master_initialize(void) {
	I2C_M_SCL = 1;
	I2C_M_SDA = 1;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint8_t i2c_master_write_register(uint32_t address, uint32_t value) {
	uint8_t ok = 0;

	// assert a start condition
	I2C_M_SDA = 0;
	i2c_master_delay();

	I2C_M_SCL = 0;
	i2c_master_delay();

	// send the CC I2C ID 
	if (i2c_master_write_byte(I2C_M_CC_WRITE) == I2C_M_ACK) {
		if (i2c_master_write_byte((uint8_t)(address >> 16)) == I2C_M_ACK) {
			if (i2c_master_write_byte((uint8_t)(address >> 8)) == I2C_M_ACK) {
				if (i2c_master_write_byte((uint8_t)address) == I2C_M_ACK) {
					ok = 1;
				}
			}
		}
	}

	ok &= i2c_master_write_byte((uint8_t)(value >> 24));
	ok &= i2c_master_write_byte((uint8_t)(value >> 16));
	ok &= i2c_master_write_byte((uint8_t)(value >> 8));
	ok &= i2c_master_write_byte((uint8_t)value);

	// make sure SDA is low so we can initiate a stop sequence
	I2C_M_SDA = 0;
	i2c_master_delay();

	// Assert a stop condition
	I2C_M_SCL = 1;
	i2c_master_delay();

	I2C_M_SDA = 1;
	i2c_master_delay();

	return ok;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint8_t i2c_master_read_register(uint32_t address, uint32_t *value) {
	uint8_t ok = 0;
	uint32_t register_value = 0U;

	// assert a start condition
	I2C_M_SDA = 0;
	i2c_master_delay();

	I2C_M_SCL = 0;
	i2c_master_delay();

	// send the CC I2C ID 
	if (i2c_master_write_byte(I2C_M_CC_WRITE) == I2C_M_ACK) {
		if (i2c_master_write_byte((uint8_t)(address >> 16)) == I2C_M_ACK) {
			if (i2c_master_write_byte((uint8_t)(address >> 8)) == I2C_M_ACK) {
				if (i2c_master_write_byte((uint8_t)address) == I2C_M_ACK) {
					ok = 1;
				}
			}
		}
	}

	// make sure SDA is low so we can initiate a stop sequence
	I2C_M_SDA = 0;
	i2c_master_delay();

	// Assert a stop condition
	I2C_M_SCL = 1;
	i2c_master_delay();

	I2C_M_SDA = 1;
	i2c_master_delay();

	if (ok == 1) {
		// assert a start condition
		I2C_M_SDA = 0;
		i2c_master_delay();
	
		I2C_M_SCL = 0;
		i2c_master_delay();
	
		// send the CC I2C ID 
		if (i2c_master_write_byte(I2C_M_CC_READ) == I2C_M_ACK) {
			uint8_t value = 0;

			ok = i2c_master_read_byte(0, &value);

			if (ok == 1) {
				register_value = ((uint32_t)value) << 24;
				ok = i2c_master_read_byte(0, &value);
			}

			if (ok == 1) {
				register_value |= ((uint32_t)value) << 16;
				ok = i2c_master_read_byte(0, &value);
			}

			if (ok == 1) {
				register_value |= ((uint32_t)value) << 8;
				ok = i2c_master_read_byte(1, &value);
			}

			if (ok == 1) {
				register_value |= ((uint32_t)value);
			}
		}
	}

	I2C_M_SDA = 0;
	i2c_master_delay();

	// Assert a stop condition
	I2C_M_SCL = 1;
	i2c_master_delay();

	I2C_M_SDA = 1;
	i2c_master_delay();

	*value = register_value;

	return ok;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint8_t i2c_master_write_byte(uint8_t value) {
	uint8_t ack = I2C_M_NACK;
	uint8_t mask = 0x80;

	while (mask != 0) {
		if ((value & mask) != 0x00) {
			I2C_M_SDA = 1;

		} else {
			I2C_M_SDA = 0;
		}

		mask >>= 1;

		I2C_M_SCL = 1;
		i2c_master_delay();

		i2c_master_delay();

		I2C_M_SCL = 0;
		i2c_master_delay();
	}

	// make sure SDA is released
	I2C_M_SDA = 1;

	// configure pin41 as an input
	I2C_M_DIR_SDA = 1;

	// wait a little
	i2c_master_delay();

	if (I2C_M_SDA == 0) {
		ack = I2C_M_ACK;
	}

	// configure pin41 as an output
	I2C_M_DIR_SDA = 0;

	// cycle the clock one more time
	I2C_M_SCL = 1;
	i2c_master_delay();

	i2c_master_delay();

	I2C_M_SCL = 0;
	i2c_master_delay();

	return ack;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint8_t i2c_master_read_byte(uint8_t end, uint8_t *value) {
	uint8_t mask = 0x80;
	uint8_t timeout = 0;
	uint8_t lvalue = 0x00;
	uint8_t ok = 1;

	// make sure SDA is released
	I2C_M_SDA = 1;
	// configure pin41 as an input
	I2C_M_DIR_SDA = 1;

	while (mask != 0) {
		I2C_M_SCL = 1;

		// configure pin 01 as an input
		I2C_M_DIR_SCL = 1;
		// wait for clock stetching, if any FIXME
		while (I2C_M_SCL == 0) {
			timeout++;
			if (timeout >= I2C_M_CLK_WAIT_TIMEOUT) {
				ok = 0;
				break;
			}
		}

		// configure pin 01 as an output
		I2C_M_DIR_SCL = 0;
		i2c_master_delay();

		if (I2C_M_SDA == 1) {
			lvalue |= mask;
		}

		mask >>= 1;

		I2C_M_SCL = 0;
		i2c_master_delay();
	}

	// configure pin41 as an output
	I2C_M_DIR_SDA = 0;
	if (end == 0) {
		// give an ACK
		I2C_M_SDA = 0;

	} else {
		// give a NACK
		I2C_M_SDA = 1;
	}

	// wait a little
	i2c_master_delay();

	I2C_M_SCL = 1;

	// wait for clock stetching, if any FIXME
	while (I2C_M_SCL == 0) {
		timeout++;
		if (timeout >= I2C_M_CLK_WAIT_TIMEOUT) {
			ok = 0;
			break;
		}
	}

	i2c_master_delay();

	I2C_M_SCL = 0;

	if (ok == 1) {
		*value = lvalue;
	}

	return ok;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void i2c_master_delay(void) {
	uint8_t i;

	for (i=0; i<20; ++i) {
		__no_operation();
	}
}

