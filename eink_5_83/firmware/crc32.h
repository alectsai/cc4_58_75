// crc32.h
#ifndef _CRC32_H
#define _CRC32_H

//
void crc32_init(uint32_t *crc);
void crc32_compute(uint32_t *crc, uint8_t data);


#endif // _CRC32_H
