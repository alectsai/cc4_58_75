//----------------------------------------------------------------------------
// display.h
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_SIZE_5p83		0x00
#define DISPLAY_SIZE_7p5		0x01

// The 5.83" glass is 600x448
// The 7.5" glass is 640x384

#define DISPLAY_COLOR_BW							0x00
#define DISPLAY_COLOR_BWR							0x01
#define DISPLAY_COLOR_BWY							0x02

#define DISPLAY_SCREEN_SIZE_5p83_BW				 33600UL
#define DISPLAY_SCREEN_SIZE_5p83_BWR			 67200UL
#define DISPLAY_SCREEN_SIZE_5p83_BWY			 67200UL
#define DISPLAY_SCREEN_SIZE_7p5_BW				 30720UL
#define DISPLAY_SCREEN_SIZE_7p5_BWR				 61440UL
#define DISPLAY_SCREEN_SIZE_7p5_BWY				 61440UL

#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BW	 9UL
#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWR	17UL
#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWY	17UL
#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BW		 8UL
#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWR	15UL
#define DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWY	15UL

#define DISPLAY_SCREEN_COUNT_5p83_BW				10
#define DISPLAY_SCREEN_COUNT_5p83_BWR				 7
#define DISPLAY_SCREEN_COUNT_5p83_BWY				 7
#define DISPLAY_SCREEN_COUNT_7p5_BW					10
#define DISPLAY_SCREEN_COUNT_7p5_BWR				 8
#define DISPLAY_SCREEN_COUNT_7p5_BWY				 8

//#define DISPLAY_DATA_ADDRESS_OFFSET		(0x00001000)

#define DISPLAY_INT_DATA_ADDRESS_OFFSET		(0x00001000)
#define DISPLAY_EXT_DATA_ADDRESS_OFFSET		(0x00010000)
#define DISPLAY_EXT_RLE_DATA_ADDRESS_OFFSET	(0x00090000)

#define DISPLAY_FLASH_BLOCK_SIZE	(0x1000UL)
//#define DISPLAY_SCREEN_SIZE_BW		(DISPLAY_FLASH_BLOCK_SIZE*DISPLAY_SCREEN_BLOCKS_PER_SCREEN_BW)
//#define DISPLAY_SCREEN_SIZE_BWR		(DISPLAY_FLASH_BLOCK_SIZE*DISPLAY_SCREEN_BLOCKS_PER_SCREEN_BWR)
//#define DISPLAY_SCREEN_DATA_SIZE_BW		(0x8340UL)
//#define DISPLAY_SCREEN_DATA_SIZE_BWR	(DISPLAY_SCREEN_DATA_SIZE_BW*2)

//#define DISPLAY_SCREEN_OFFSET_BWR	(DISPLAY_SCREEN_DATA_SIZE_BW)

#define DISPLAY_CONFIGURATION_ONE_DISPLAY		0x0
#define DISPLAY_CONFIGURATION_TWO_DISPLAYS_LEFT_RIGHT 0x1
#define DISPLAY_CONFIGURATION_TWO_DISPLAYS_TOP_BOTTOM 0x2

extern uint8_t display_size;
extern uint8_t display_color;
extern uint8_t display_configuration;
extern uint8_t display_num_screens;
extern uint32_t display_time_since_last_render;
extern uint8_t display_render_complete;

//#if CODE_SIZE_OPTIMIZED
typedef struct tag_type_5875 {
    uint8_t         tag_type;
    uint8_t         internal_type;
    uint8_t         color;
    uint8_t         blkPerScreen;
    uint8_t         num_screen;
    uint32_t       screen_size;
    uint32_t       screen_memory;
    uint32_t       timeout;
} __attribute__((packed)) tag_type_config5875_t;
//#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_SPI_DEVICE_CS1		1
#define DISPLAY_SPI_DEVICE_CS2		2
#define DISPLAY_SPI_DEVICE_MFCSB	4

#define DISPLAY_DC				(P1_bit.no3)
#define DISPLAY_RESET			(P2_bit.no3)
#define DISPLAY_BUSY 			(P12_bit.no1)
#define DISPLAY_SPI_CLK			(P0_bit.no0)
#define DISPLAY_SPI_MOSI		(P0_bit.no2)

#define DISPLAY_SPI_CS1			(P2_bit.no0)
#define DISPLAY_SPI_CS2			(P1_bit.no4)
//
#define DISPLAY_SPI_SCLK		(P0_bit.no0)
#define DISPLAY_SPI_SCLK_DIR	(PM0_bit.no0)
//
#define DISPLAY_SPI_MOSI		(P0_bit.no2)
#define DISPLAY_SPI_MOSI_DIR	(PM0_bit.no2)
//
#define DISPLAY_SPI_MFCSB		(P1_bit.no4)
#define DISPLAY_SPI_MFCSB_DIR	(PM1_bit.no4)
//
#define DISPLAY_SPI_FMSDO		(P12_bit.no2)


#define SPI_DATA		1
#define SPI_CMD_DATA	0



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_CLEAR_SCREEN_WHITE_VALUE 0x00
#define DISPLAY_CLEAR_SCREEN_BLACK_VALUE 0xFF



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_initialize(void);
void display_check_provision(void);
void display_configure(uint8_t type);
void display_render(uint8_t screen);
void display_power_off(void);
void display_clear_screen(uint8_t screen);
void display_copy_screen(uint32_t cmd);
void display_crc(uint32_t cmd);
void display_spi_transfer(uint8_t mode, uint8_t device, uint8_t *buffer_out, uint16_t size, uint8_t continuation);
void display_spi_read(uint8_t device, uint8_t *buffer_in, uint16_t size, uint8_t continuation);
void display_spi_transfer_fill(uint8_t device, uint8_t fill, uint32_t count, uint8_t continuation);

