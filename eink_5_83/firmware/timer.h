//----------------------------------------------------------------------------
// timer.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern volatile uint8_t timer_increment;


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void timer_initialize(void);
void timer_delay(uint16_t milliseconds);
void busyWait(uint8_t numUS);
