//----------------------------------------------------------------------------
// all_includes.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
// In tools directory
//----------------------------------------------------------------------------
#include "ior5f1037a.h"
#include "ior5f1037a_ext.h"
#include "intrinsics.h"
#include "stdbool.h"
#include <stdint.h>



//----------------------------------------------------------------------------
// In project directory
//----------------------------------------------------------------------------
#include "event_manager.h"
#include "timer.h"
#include "spi.h"
//#include "eeprom.h"
#include "flash.h"
#include "display.h"
#include "i2c_master.h"
#include "i2c_slave.h"
#include "communications.h"
#include "common.h"
#include "error.h"


#define DEBUG_0	(P4_bit.no0)
#define DEBUG_1	(P4_bit.no2)

#define CODE_SIZE_OPTIMIZED     1

