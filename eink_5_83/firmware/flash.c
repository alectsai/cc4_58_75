//----------------------------------------------------------------------------
// flash.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint32_t flash_memory_size;
uint16_t flash_page_size;
uint8_t flash_power_state;
uint8_t flash_power_timeout;

void flash_write_page(uint32_t address, uint8_t *buffer, uint16_t size);


//----------------------------------------------------------------------------
// Initialize the memory device
// the caller should evaluate the memory device size
//----------------------------------------------------------------------------
void flash_initialize(void) {
	uint8_t command[4];

	flash_page_size = 256;

	// assume the smallest size first - 4M bit
	flash_memory_size = FLASH_DEVICE_SIZE_4_MBIT;

	// wake the flash in case it is asleep
	flash_power(FLASH_POWER_ON);

	// Read flash manufacturer ID
	command[0] = FLASH_CMD_RD_ID;
	command[1] = 0;				 
	command[2] = 0;				 
	command[3] = 0;				 
	spi_transfer(SPI_DEVICE_MEMORY_CS, command,command,4,SPI_TRANSFER_NO_CONTINUATION, 0);

	if (command[1] == FLASH_ID_ADESTO) {
		switch(command[2]) {
			case FLASH_ID_ADESTO_256_KBIT:
				flash_memory_size = FLASH_DEVICE_SIZE_256_KBIT;
				break;
			case FLASH_ID_ADESTO_512_KBIT:
				flash_memory_size = FLASH_DEVICE_SIZE_512_KBIT;
				break;
			case FLASH_ID_ADESTO_1_MBIT:
				flash_memory_size = FLASH_DEVICE_SIZE_1_MBIT;
				break;
			case FLASH_ID_ADESTO_2_MBIT:
				flash_memory_size = FLASH_DEVICE_SIZE_2_MBIT;
				break;
			case FLASH_ID_ADESTO_4_MBIT:
				flash_memory_size = FLASH_DEVICE_SIZE_4_MBIT;
				break;
			default:
				error_report(ERROR_FLASH_WRONG_TYPE);
		}

	} else {
		// type not supported 
		error_report(ERROR_FLASH_WRONG_TYPE);
	}

	__disable_interrupt();
	flash_power_timeout = 0;
	__enable_interrupt();

	flash_power(FLASH_POWER_OFF);
}



//----------------------------------------------------------------------------
// flash_power - turn on and off the flash ultra low power mode
//	Everytime the chip return from ultra low power mode the sofware write enble is reactivated
//	so we will have to disable it if we are writing to the chip
//----------------------------------------------------------------------------
void flash_power(uint8_t state) {
	uint8_t command[2];

	if (state == FLASH_POWER_ON) {
		if (flash_power_state != FLASH_POWER_ON) {

			// To wake from utra-deep power down mode, assert the CS signal for at least
			// 70 ms.  The CS can either be deasserted immediately or a new command issued.
			// if a command is issues before the 70 ms has occurred, the command will (probably)
			// be ignored
			SPI_MEMORY_CS = 0;
			// let's give it 5% leeway
			busyWait(74);
			//
			SPI_MEMORY_CS = 1;


			// Enable writing
			command[0] = FLASH_CMD_WR_EN;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION, 0);
	
			// disable the global write protection
			// this is re-enabled by default after waking from ultra-deep sleep
			command[0] = FLASH_CMD_WR_STATUS;
			command[1] = 0;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION, 0);

			command[0] = FLASH_CMD_RD_STATUS;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION, 0);
			if ((command[1] & 0xc) != 0) {
				error_report(ERROR_FLASH_WR_PROTECT);
			}

			flash_power_state = FLASH_POWER_ON;
		}

		flash_power_timeout = FLASH_POWER_TIMEOUT_COUNT;

	} else {
		if (flash_power_state != FLASH_POWER_OFF) {
			__disable_interrupt();

			if (flash_power_timeout == 0) {
				// put device into Ultra-Deep Power-Down mode
				command[0] = FLASH_CMD_PWR_DN;
				spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION, 0);

				flash_power_state = FLASH_POWER_OFF;
			}
	
			__enable_interrupt();
		}
	}
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void flash_write(uint32_t address, uint8_t *buffer, uint16_t size) {
	// detect if write go beyond boundary if it is true then break the write up according
	// to page boundary

	uint32_t mask = ~0xFF;


	// the write spill into another page
	if ((address & mask) != ((address +size-1) & mask)) {
		uint32_t nextPageAdr = (address & mask) + 0x100;
		uint16_t sizeToEndOPage = nextPageAdr - address;
		flash_write_page(address, buffer, sizeToEndOPage);
		flash_write_page(nextPageAdr, buffer+sizeToEndOPage, size - sizeToEndOPage);
		 
	} else {
		flash_write_page(address, buffer, size);
	}

	//pageCnt++;
	//curSize = curSize - 256;
	
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void flash_write_page(uint32_t address, uint8_t *buffer, uint16_t size) {
	uint8_t command[4];
	uint8_t timeout;

	flash_power(FLASH_POWER_ON);
	// check to see if we are writing the provisioning file
	if (address == 0) {
		// erase the provision area
		erase_prov_area();
	}

	// Send WREN command
	command[0] = FLASH_CMD_WR_EN;
	spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION, 0);

	// Write data
	command[0] = FLASH_CMD_WR;
	if (flash_memory_size > (64L * 1024L)) {
		command[1] = (address >> 16);
		command[2] = (address >> 8);
		command[3] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_CONTINUATION, 0);

	} else {
		command[1] = (address >> 8);
		command[2] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,3,SPI_TRANSFER_CONTINUATION, 0);
	}
	
	// data 
	spi_transfer(SPI_DEVICE_MEMORY_CS,buffer,NULL,size,SPI_TRANSFER_NO_CONTINUATION, 1);
	DISPLAY_SPI_CLK = 0;

	// Poll for completion
	timeout = 0;
	while (1) {
		command[0] = FLASH_CMD_RD_STATUS;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION, 0);
		if ((command[1] & 0x01) == 0) {
			break;
		}

		++timeout;
		if (timeout > 10) {
			// 10ms maximum wait
			break;
		}

	}
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void flash_read(uint32_t address, uint8_t *buffer, uint16_t size) {
	uint8_t command[4];

	flash_power(FLASH_POWER_ON);

	command[0] = FLASH_CMD_RD;
	if (flash_memory_size > (64L * 1024L)) {
		command[1] = (address >> 16);
		command[2] = (address >> 8);
		command[3] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_CONTINUATION, 0);

	} else {
		command[1] = (address >> 8);
		command[2] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,3,SPI_TRANSFER_CONTINUATION, 0);
	}

	//data
	spi_transfer(SPI_DEVICE_MEMORY_CS,NULL,buffer,size,SPI_TRANSFER_NO_CONTINUATION, 1);
}


//----------------------------------------------------------------------------
// eraseProvArea() - erase the provision area from 0 to 0x7FF
//----------------------------------------------------------------------------
void erase_prov_area() {
	for (int16_t i=0; i<8; i++) {
		flash_page_erase (i * 0x100);
	}
}

//----------------------------------------------------------------------------
// This will erase the page of data indicated by the higher 3 bytes of the address
// The last byte of the address is not being used by the flash
//----------------------------------------------------------------------------
void flash_page_erase(uint32_t pageAdr) {
	uint8_t command[4];
	uint8_t timeout;

	flash_power(FLASH_POWER_ON);
	
	// Write enable
	command[0] = FLASH_CMD_WR_EN;			 
	spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION, 0);

	// erase 256 bytes at the address
	// the last byte of the address is ignored by the flash
	command[0] = FLASH_CMD_ERASE_PAGE;
	command[1] = (pageAdr >> 16);
	command[2] = (pageAdr >> 8);
	command[3] = 0;			 

	spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_NO_CONTINUATION, 0);

		// Poll for completion
	timeout = 0;
	while (1) {
		command[0] = FLASH_CMD_RD_STATUS;
		command[1] = 0;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION, 0);
		if ((command[1] & 0x01) == 0) {
			break;
		}

		++timeout;
		if (timeout > 10) {
			// 10ms maximum wait
			break;
		}

		timer_delay(1);
	}

}


//----------------------------------------------------------------------------
// This will erase the block of data indicated by the block size and index 
//----------------------------------------------------------------------------
void flash_erase(uint32_t blockSize, uint32_t block, uint32_t count) {
	uint8_t command[4];
	uint16_t timeout;
	uint16_t i;

	uint32_t pageAddr = blockSize * block;

	flash_power(FLASH_POWER_ON);

	for(i=0;i<count;i++) {
		// Write enable
		command[0] = FLASH_CMD_WR_EN;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION, 0);

		switch (blockSize) {
			case FLASH_BLOCK_SIZE_256:
				command[0] = FLASH_CMD_ERASE_PAGE;
				timeout = 26;
				break;
			case FLASH_BLOCK_SIZE_4K:
				command[0] = FLASH_CMD_ERASE_BLK_4K;
				timeout = 50;
				break;
			case FLASH_BLOCK_SIZE_32K:
				command[0] = FLASH_CMD_ERASE_BLK_32K;
				timeout = 400;
				break;
		}

		// the last byte of the address is ignored by the flash anyway
		command[1] = (pageAddr >> 16);
		command[2] = (pageAddr >> 8);
		command[3] = 0;
	
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_NO_CONTINUATION, 0);

		// Poll for completion
		while (1) {
			command[0] = FLASH_CMD_RD_STATUS;
			command[1] = 0;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION, 0);
	
			if ((command[1] & 0x01) == 0) {
				// FIXME - no indication of erase failure!
				break;
			}
	
			--timeout;
			if (timeout == 0) {
				// timed out
				// FIXME - no indication of timeout failure!
				break;
			}

			timer_delay(1);
		}

		// increment the pageAddr by 1 block
		pageAddr += blockSize;
	}

}
