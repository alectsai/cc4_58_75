//----------------------------------------------------------------------------
// display.c
//----------------------------------------------------------------------------
#include "all_includes.h"
#include <string.h>
#include "uc.h"
#include "crc32.h"


// address are in decimal because that's how they are specified to us
#define UC8159_583_ADDR_PLL_TABLE	25616UL
#define UC8159_75_ADDR_PLL_TABLE	25039UL
#define UC8159_583_ADDR_VCOM_TABLE	25600UL
#define UC8159_75_ADDR_VCOM_TABLE	25049UL
#define UC8159_ADDR_TMP_TABLE		25002UL
#define UC8159_75_ADDR_LOT_NUM		25035UL

#define TIMEOUT_POWERON		 16	//	4 seconds
#define TIMEOUT_POWEROFF	 16	//	4 seconds
#define TIMEOUT_PSR			 32 //	4 seconds
#define TIMEOUT_RENDER		240	// 60 seconds
#define TIMEOUT_TCON		  1
#define TIMEOUT_TSC			  1
#define TIMEOUT_TRES		  1

#define TIMEOUT_POWER_UP	16	// 4 seconds

#define DISPLAY_BUFFER_SIZE	4

//#define DISPLAY_DEEP_SLEEP


#define DISPLAY_VALUE_DEFAULT_TCON	0x22
//
// 0x17 - black border
// 0x77	- white border
// 0x37	- Gray1 border
// 0x57	- Gray2 border
#define DISPLAY_VALUE_DEFAULT_CDI	0x77
//	
#define DISPLAY_VALUE_DEFAULT_BTST0	0xC7
#define DISPLAY_VALUE_DEFAULT_BTST1	0xCC
#define DISPLAY_VALUE_DEFAULT_BTST2	0x15
//	
#define DISPLAY_VALUE_DEFAULT_PWR0	0x37
#define DISPLAY_VALUE_DEFAULT_PWR1	0x00
//#define DISPLAY_VALUE_DEFAULT_PWR2	0x05
//#define DISPLAY_VALUE_DEFAULT_PWR3	0x05
//	
#define DISPLAY_VALUE_DEFAULT_PSR0	0xCF
#define DISPLAY_VALUE_DEFAULT_PSR1	0x08
//	
#define DISPLAY_VALUE_DEFAULT_PWS	0xEE

#define DISPLAY_DISABLE				0
#define DISPLAY_ENABLE				1

#define DISPLAY_RENDER_INITIAL_WAIT	1900
#define DISPLAY_RENDER_TIMEOUT_BW	(40000 - DISPLAY_RENDER_INITIAL_WAIT)
#define DISPLAY_RENDER_TIMEOUT_BWR	(90000 - DISPLAY_RENDER_INITIAL_WAIT)
#define DISPLAY_RENDER_TIMEOUT_BWY	(180000 - DISPLAY_RENDER_INITIAL_WAIT)


#define PIXEL_BLACK_HIGH	0x00
#define PIXEL_BLACK_LOW		0x00
#define PIXEL_WHITE_HIGH	0x30
#define PIXEL_WHITE_LOW		0x03
#define PIXEL_RED_0_HIGH	0x40
#define PIXEL_RED_0_LOW		0x04
#define PIXEL_RED_1_HIGH	0x50
#define PIXEL_RED_1_LOW		0x05
#define PIXEL_RED_2_HIGH	0x60
#define PIXEL_RED_2_LOW		0x06

//----------------------------------------------------------------------------
void display_enable(uint8_t enable);
void debug(int16_t count);
void swapBytes (uint8_t *arryPtr, uint8_t len);


//----------------------------------------------------------------------------
uint8_t display_valid;
uint8_t display_size;
uint32_t display_screen_size_image;
uint32_t display_screen_size_memory;
uint8_t display_color;
uint8_t display_configuration;
uint8_t display_num_screens;
uint32_t display_time_since_last_render;
uint8_t display_render_complete;

uint32_t display_render_timeout;

uint8_t display_temperature_range[10];
uint8_t display_pll_values[10];
uint8_t display_vcom_values[10];

uint8_t display_cmd_buf[16];
//uint8_t display_vdcs_read_value;

uint8_t display_blocks_per_screen;
uint32_t display_color_screen_offset;

#if CODE_SIZE_OPTIMIZED
const tag_type_config5875_t tag_type_config[6] = {
    {0x9F, DISPLAY_SIZE_5p83, DISPLAY_COLOR_BW,  DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BW, 10, 33600UL, DISPLAY_FLASH_BLOCK_SIZE*9UL, DISPLAY_RENDER_TIMEOUT_BW},
    {0xBC, DISPLAY_SIZE_5p83, DISPLAY_COLOR_BWR, DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWR, 7, 33600UL, DISPLAY_FLASH_BLOCK_SIZE*17UL, DISPLAY_RENDER_TIMEOUT_BWR}, 
    {0xAC, DISPLAY_SIZE_5p83, DISPLAY_COLOR_BWY, DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWY, 7, 33600UL, DISPLAY_FLASH_BLOCK_SIZE*17UL, DISPLAY_RENDER_TIMEOUT_BWY}, 
    {0xD3, DISPLAY_SIZE_7p5,  DISPLAY_COLOR_BW,  DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BW, 10, 30720UL, DISPLAY_FLASH_BLOCK_SIZE*8UL, DISPLAY_RENDER_TIMEOUT_BW},
    {0xD5, DISPLAY_SIZE_7p5,  DISPLAY_COLOR_BWR, DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWR, 8, 30720UL, DISPLAY_FLASH_BLOCK_SIZE*15UL, DISPLAY_RENDER_TIMEOUT_BWR},
    {0xA3, DISPLAY_SIZE_7p5,  DISPLAY_COLOR_BWY, DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWY, 8, 30720UL, DISPLAY_FLASH_BLOCK_SIZE*15UL, DISPLAY_RENDER_TIMEOUT_BWY}, 
};
#endif

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_initialize(void) {
	display_time_since_last_render = 0;
	display_render_complete = 0;
    display_valid = 0;
}

//----------------------------------------------------------------------------
void display_check_provision(void)
{
    uint32_t    value;

    if( display_valid == 1 )    // Check to see if we already setup the display
    {
        return;
    }

	if (i2c_master_read_register(0x0000,&value) != I2C_M_NACK)
    {
#if CODE_SIZE_OPTIMIZED
       uint8_t type = (value >> 8) & 0xFF;
	tag_type_config5875_t *cfg;
	for(int i = 0; i < 6; i++) {
        cfg = (tag_type_config5875_t*)&tag_type_config[i];
	    if(type == cfg->tag_type) {
            //g_tagType               = cfg->tag_type;
            display_size             = cfg->internal_type;
            display_color           = cfg->color;
 	        break;
	    }
	}

#else	// Assign display type based on tag type read from CC4
		switch ((value >> 8) & 0xFF)
        {
			case 0x9F:
				// BW
				display_size = DISPLAY_SIZE_5p83;
				break;
			case 0xBC:
				// BWR
				display_size = DISPLAY_SIZE_5p83;
				display_color = DISPLAY_COLOR_BWR;
				break;
			case 0xAC:
				// BWY (treat it just like a BWR)
				display_size = DISPLAY_SIZE_5p83;
				display_color = DISPLAY_COLOR_BWY;
				break;

			case 0xD3:
				// BW
				display_size = DISPLAY_SIZE_7p5;
				break;
			case 0xD5:
				// BWR
				display_size = DISPLAY_SIZE_7p5;
				display_color = DISPLAY_COLOR_BWR;
				break;
			case 0xA3:
				// BWY (treat it just like a BWR)
				display_size = DISPLAY_SIZE_7p5;
				display_color = DISPLAY_COLOR_BWY;
				break;

			default:
				error_report(ERROR_INVALID_TYPE);
				return;
		}
#endif // code size		
	}
    else
    {
		error_report(ERROR_COM_NO_CC4);
        return;
	}

    display_valid = 1;

    display_configure((value>>8)&0x0FFU);

	value = ((uint32_t)display_configuration << 12) | ((uint32_t)display_color << 8) | (uint32_t)display_size;
	communications_set_register(REGISTER_DISPLAY_TYPE,value);

    // Clear any previous ERROR_INVALID_TYPE

    value = communications_get_register( REGISTER_STATUS );
    value &= ~ERROR_INVALID_TYPE;
    communications_set_register( REGISTER_STATUS, value );

    return;
}

//----------------------------------------------------------------------------
void display_configure(uint8_t type) {
	uint32_t vcom_addr = 0;
	uint32_t pll_addr = 0;
//	display_color_screen_offset = 0;

#if CODE_SIZE_OPTIMIZED
	tag_type_config5875_t *cfg;
	for(int i = 0; i < 6; i++) {
        cfg = (tag_type_config5875_t*)&tag_type_config[i];
	    if(type == cfg->tag_type) {
            //g_tagType               = cfg->tag_type;
            display_size             = cfg->internal_type;
            display_color           = cfg->color;
            display_num_screens     = cfg->num_screen;
            display_screen_size_image    = cfg->screen_size;
            display_blocks_per_screen     = cfg->blkPerScreen;
	     display_screen_size_memory = cfg->screen_memory;	
	     display_render_timeout = cfg->timeout;
//            if (display_color == DISPLAY_COLOR_BWR)
//            	dispImageSizeInFlash = cfg->blkPerScreen * DISPLAY_FLASH_BLOCK_SIZE * 2;
//            else
//            	dispImageSizeInFlash = cfg->blkPerScreen * DISPLAY_FLASH_BLOCK_SIZE;
	        break;
	    }
	}

#else	// Assign display type based on tag type read from CC4
	switch (type) {
		case 0x9F:
			// BW
			display_size = DISPLAY_SIZE_5p83;
			display_num_screens = DISPLAY_SCREEN_COUNT_5p83_BW;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BW;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_5p83_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BW * DISPLAY_FLASH_BLOCK_SIZE;
			break;
		case 0xBC:
			// BWR
			display_size = DISPLAY_SIZE_5p83;
			display_color = DISPLAY_COLOR_BWR;
			display_num_screens = DISPLAY_SCREEN_COUNT_5p83_BWR;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWR;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_5p83_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWR * DISPLAY_FLASH_BLOCK_SIZE;
			break;
		case 0xAC:
			// BWY (treat it just like a BWR)
			display_size = DISPLAY_SIZE_5p83;
			display_color = DISPLAY_COLOR_BWY;
			display_num_screens = DISPLAY_SCREEN_COUNT_5p83_BWY;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWY;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_5p83_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_5p83_BWY * DISPLAY_FLASH_BLOCK_SIZE;
			break;

		// these are temporary tag types
		case 0xD3:
			// BW
			display_size = DISPLAY_SIZE_7p5;
			display_num_screens = DISPLAY_SCREEN_COUNT_7p5_BW;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BW;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_7p5_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BW * DISPLAY_FLASH_BLOCK_SIZE;
			break;
		case 0xD5:
			// BWR
			display_size = DISPLAY_SIZE_7p5;
			display_color = DISPLAY_COLOR_BWR;
			display_num_screens = DISPLAY_SCREEN_COUNT_7p5_BWR;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWR;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_7p5_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWR * DISPLAY_FLASH_BLOCK_SIZE;
			break;
		case 0xA3:
			// BWY (treat it just like a BWR)
			display_size = DISPLAY_SIZE_7p5;
			display_color = DISPLAY_COLOR_BWY;
			display_num_screens = DISPLAY_SCREEN_COUNT_7p5_BWY;
			display_blocks_per_screen = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWY;
			display_screen_size_image = DISPLAY_SCREEN_SIZE_7p5_BW;
			display_screen_size_memory = DISPLAY_SCREEN_BLOCKS_PER_SCREEN_7p5_BWR * DISPLAY_FLASH_BLOCK_SIZE;
			break;

		default:
			error_report(ERROR_INVALID_TYPE);
			break;
	}


	if (display_color == DISPLAY_COLOR_BW) {
		display_render_timeout = DISPLAY_RENDER_TIMEOUT_BW;

	} else if (display_color == DISPLAY_COLOR_BWR) {
		display_render_timeout = DISPLAY_RENDER_TIMEOUT_BWR;

	} else {
		// The 5.83" BWY display takes 2x longer to render than the BWR display
		display_render_timeout = DISPLAY_RENDER_TIMEOUT_BWY;
	}
#endif // code_size

	// read the VCOM, PLL, and Temperature data from the display flash
	display_enable(DISPLAY_ENABLE);

	// Send unknown cmd to FLASH per UC example code
	// this might allow the pass-through mode, maybe, ask again!
	display_cmd_buf[0] = 0xE5;
	display_cmd_buf[1] = 0x03;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);


	// 
	// read the VCOM data from the FLASH device
	// 
	// Enable SPI FLASH pass-thru
	display_cmd_buf[0] = UC_CMD_65_DAM;
	display_cmd_buf[1] = 0x01;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
	timer_delay(1);


	// bring the display's FLASH chip out of deep sleep in case it is..
	display_cmd_buf[0] = UC_CMD_AB_FLASH_WAKE;
	display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);
	// there needs to be a 3 us delay after this command
	// the current delay between CS assertions is already about 33 us without additional
	// delays, so we should be good.


	if (display_size == DISPLAY_SIZE_5p83) {
		vcom_addr = UC8159_583_ADDR_VCOM_TABLE;
		pll_addr = UC8159_583_ADDR_PLL_TABLE;
	} else {
		vcom_addr = UC8159_75_ADDR_VCOM_TABLE;
		pll_addr = UC8159_75_ADDR_PLL_TABLE;
                // read the lot number from the table at 0x0061CB
                display_cmd_buf[0] = 0x03;
                display_cmd_buf[1] = (uint8_t)(UC8159_75_ADDR_LOT_NUM>>16);
                display_cmd_buf[2] = (uint8_t)(UC8159_75_ADDR_LOT_NUM>>8);
                display_cmd_buf[3] = (uint8_t)UC8159_75_ADDR_LOT_NUM;
                display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,4,SPI_TRANSFER_CONTINUATION);
                display_spi_read(DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,4,SPI_TRANSFER_NO_CONTINUATION);
                swapBytes(&display_cmd_buf[0], 4);
                uint32_t *ptr = (uint32_t *)&display_cmd_buf[0];
                communications_set_register( REGISTER_LOT_NUM, *ptr );
	}

	// read the temperature table from the table at 0x0061AA
	display_cmd_buf[0] = 0x03;
	display_cmd_buf[1] = (uint8_t)(UC8159_ADDR_TMP_TABLE>>16);
	display_cmd_buf[2] = (uint8_t)(UC8159_ADDR_TMP_TABLE>>8);
	display_cmd_buf[3] = (uint8_t)UC8159_ADDR_TMP_TABLE;
	display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,4,SPI_TRANSFER_CONTINUATION);
	display_spi_read(DISPLAY_SPI_DEVICE_MFCSB,display_temperature_range,10,SPI_TRANSFER_NO_CONTINUATION);

	// read the PLL data from the table at 0x006410
	display_cmd_buf[0] = 0x03;
	display_cmd_buf[1] = pll_addr>>16;
	display_cmd_buf[2] = pll_addr>>8;
	display_cmd_buf[3] = pll_addr;
	display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,4,SPI_TRANSFER_CONTINUATION);
	display_spi_read(DISPLAY_SPI_DEVICE_MFCSB,display_pll_values,10,SPI_TRANSFER_NO_CONTINUATION);



	// read the PLL data from the table at 0x006410
	display_cmd_buf[0] = 0x03;
	display_cmd_buf[1] = vcom_addr>>16;
	display_cmd_buf[2] = vcom_addr>>8;
	display_cmd_buf[3] = vcom_addr;
	display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,4,SPI_TRANSFER_CONTINUATION);
	display_spi_read(DISPLAY_SPI_DEVICE_MFCSB,display_vcom_values,10,SPI_TRANSFER_NO_CONTINUATION);




	// Disable SPI Flash pass-thru
	display_cmd_buf[0] = UC_CMD_65_DAM;
	display_cmd_buf[1] = 0x00;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);

	display_enable(DISPLAY_DISABLE);
}

int display_wait(uint8_t duration) {
    // Wait for BUSY bit to go high
    uint8_t k = timer_increment;
    while (DISPLAY_BUSY == 0) {
        // Save some power on the MCU. Let the timer wake it up every quarter second.
        __halt();
        WDTE = 0xAC; // Service the WDT
        if ((timer_increment - k) >= duration) {
            return 1;
	}
    }
    return 0;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_render(uint8_t screen) {
	int32_t i;
	int32_t j;
//	uint8_t k;
	uint8_t byte_black;
	uint8_t byte_color;
	uint8_t image_in_data[DISPLAY_BUFFER_SIZE];
	uint8_t image_in_data_color[DISPLAY_BUFFER_SIZE];
	uint32_t address;
	uint32_t address_color;
	uint32_t status;
	uint32_t size;
	uint32_t render_count;
	uint8_t device;
	uint8_t temperature;
	uint8_t *in_ptr_black;
	uint8_t *in_ptr_color;
	uint8_t *out_ptr;

	uint32_t error = 0x00000000;
	uint8_t pll_read_value;
	uint8_t vcom_read_value;

	uint32_t register_0180;
	uint32_t register_0184;
	uint16_t current_voltage_reading;

	uint8_t lpd_value;

	current_voltage_reading = 0xFF;


/*
	// We are representing the temperature at 2x the normal temperature
	//		That is, 25C has a value of 50 or 0x32
	// The temperature is used to calculate a calibration factor for the
	// UC8159 and also to look up the correct PLL value for the corresponding
	// temperature
	i = (int32_t)communications_get_register(REGISTER_TEMPERATURE);
	i = ((i + 250) / 500);
	temperatureX2 = (int8_t)i;
*/


	// Power on the FLASH
	flash_power(FLASH_POWER_ON);

	display_enable(DISPLAY_ENABLE);

	// Set "Rendering" status
	communications_set_register( REGISTER_STATUS, (communications_get_register(REGISTER_STATUS)&STATUS_RENDER_STATE_MASK) | STATUS_RENDER_STATE_RENDERING );

	// Configure Power Setting
	display_cmd_buf[0] = UC_CMD_01_PWR;
	display_cmd_buf[1] = DISPLAY_VALUE_DEFAULT_PWR0;
	display_cmd_buf[2] = DISPLAY_VALUE_DEFAULT_PWR1;
	//display_cmd_buf[3] = DISPLAY_VALUE_DEFAULT_PWR2;
	//display_cmd_buf[4] = DISPLAY_VALUE_DEFAULT_PWR3;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,3,SPI_TRANSFER_NO_CONTINUATION);


	// Configure Panel Setting
	display_cmd_buf[0] = UC_CMD_00_PSR;
	display_cmd_buf[1] = DISPLAY_VALUE_DEFAULT_PSR0;
	display_cmd_buf[2] = DISPLAY_VALUE_DEFAULT_PSR1;
	//display_cmd_buf[1] = 0xCF; // scan up, shift right, changing this doesn't rotate top to bottom..
	//display_cmd_buf[2] = 0x08;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,3,SPI_TRANSFER_NO_CONTINUATION);
	timer_delay(1);
	//
	i = 20;
	while (DISPLAY_BUSY == 0) {
		if ((i--)==0) {
			error = 0x81000000;
			goto render_end;
		} 
		// do nothing but wait
		timer_delay(1);
	}


	// Send unknown cmd to CU8159 per UC example code
	// The assumption is that this command directs the UC8159 to read the lookup data from the attached FLASH
	display_cmd_buf[0] = UC_CMD_E5_FLASH_READ;
	display_cmd_buf[1] = 0x03;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);

	// Booster Soft Start
	display_cmd_buf[0] = UC_CMD_06_BTST;
	display_cmd_buf[1] = DISPLAY_VALUE_DEFAULT_BTST0;
	display_cmd_buf[2] = DISPLAY_VALUE_DEFAULT_BTST1;
	display_cmd_buf[3] = DISPLAY_VALUE_DEFAULT_BTST2;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,4,SPI_TRANSFER_NO_CONTINUATION);



	// read the temperature
	display_cmd_buf[0] = UC_CMD_40_TSC;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_CONTINUATION);
	// wait a bit
	timer_delay(10);
	i = 100;
	while (DISPLAY_BUSY == 0) {
		// Service the WDT
		WDTE = 0xAC;

		if ((i--)==0) {
			error |= ERROR_RENDER_TIMEOUT_TSC;
			goto render_end;
		}
		// do nothing but wait
		timer_delay(1);
	}

	display_spi_read(DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
	// lose the highest bit of temp from byte 1 and the lowest two from byte 2
        display_cmd_buf[0] <<= 1;
        display_cmd_buf[1] >>= 7;
        display_cmd_buf[1] &= 0x01;
	temperature = (display_cmd_buf[0] | display_cmd_buf[1]);
        
       /*example to get temperature value from display sensor
        temp1=EPD_W21_ReadDATA();
        temp2=EPD_W21_ReadDATA();
        temp1=(temp1<<1);
        temp2=(temp2>>7);
        temp2=(temp2&0x01);
        temp=(temp1|temp2);*/


	// find the correct PLL value for the temperature based upon the CC4 temp sensor
	if (temperature<display_temperature_range[0]) {
		i = 0;
	} else if (temperature<display_temperature_range[1]) {
		i = 1;
	} else if (temperature<display_temperature_range[2]) {
		i = 2;
	} else if (temperature<display_temperature_range[3]) {
		i = 3;
	} else if (temperature<display_temperature_range[4]) {
		i = 4;
	} else if (temperature<display_temperature_range[5]) {
		i = 5;
	} else if (temperature<display_temperature_range[6]) {
		i = 6;
	} else if (temperature<display_temperature_range[7]) {
		i = 7;
	} else if (temperature<display_temperature_range[8]) {
		i = 8;
	} else {
		i = 9;
	}

	pll_read_value = display_pll_values[i];
	vcom_read_value = display_vcom_values[0];
        
        communications_set_register(REGISTER_RESERVE_1,((uint32_t)i << 24 | pll_read_value | (uint32_t)(vcom_read_value << 8) | (uint32_t)((uint32_t)temperature << 16)));

	// Configure PLL using the data from the PLL LUT
	display_cmd_buf[0] = UC_CMD_30_PLL;
	display_cmd_buf[1] = pll_read_value;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);



	// Vcom and data interval setting
	display_cmd_buf[0] = UC_CMD_50_CDI;
	display_cmd_buf[1] = DISPLAY_VALUE_DEFAULT_CDI;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);

	// TCON Resolution
	display_cmd_buf[0] = UC_CMD_61_TRES;
	if (display_size == DISPLAY_SIZE_5p83) {
		// 5.83" glass
		display_cmd_buf[1] = 0x02;
		display_cmd_buf[2] = 0x58;
		display_cmd_buf[3] = 0x01;
		display_cmd_buf[4] = 0xC0;
	} else {
		// 7.5" glass
		display_cmd_buf[1] = 0x02;
		display_cmd_buf[2] = 0x80;
		display_cmd_buf[3] = 0x01;
		display_cmd_buf[4] = 0x80;
	}
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,5,SPI_TRANSFER_NO_CONTINUATION);
	//
	// BUSY goes low immediately after last bit of 2nd byte of command clocked in
	// On my board, BUSY was low for 20.1 ms
	//

	timer_delay(15);
	i = 3500;
	while (DISPLAY_BUSY == 0) {
		if ((i--)==0) {
			error |= ERROR_RENDER_TIMEOUT_TRES;
			goto render_end;
		}

		// do nothing but wait
		timer_delay(1);

		// Service the WDT
		WDTE = 0xAC;
	}

	// PON must be issues to read the internal temperature sensor
	// Power ON and wait for busy high
	display_cmd_buf[0] = UC_CMD_04_PON;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);
	timer_delay(30);
#if 0
	i = 400;
	while (DISPLAY_BUSY == 0) {
		if ((i--)==0) {
			error |= ERROR_RENDER_TIMEOUT_POWERON;
			goto render_end;
		}

		// do nothing but wait
		timer_delay(1);
	}
#else
        if(display_wait(TIMEOUT_POWER_UP)) {
               error |= ERROR_RENDER_TIMEOUT_POWERON;
         }
#endif

	//// VCM_DC Setting
	display_cmd_buf[0] = UC_CMD_82_VDCS;
	display_cmd_buf[1] = vcom_read_value;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);




	// start sending display data
	display_cmd_buf[0] = UC_CMD_10_DTM1;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);


	address = DISPLAY_INT_DATA_ADDRESS_OFFSET + ((uint32_t)screen)*display_screen_size_memory;

	if (display_color == DISPLAY_COLOR_BW) {
		address_color = address;
	} else {
		address_color = address + display_screen_size_image;
	}

	device = DISPLAY_SPI_DEVICE_CS1;

	size = display_screen_size_image;

	while (size) {
		// here we must convert from 1bpp to 2bpp (each bit is sent twice)
		flash_read(address,image_in_data,4);
		address += 4UL;

		if (display_color != DISPLAY_COLOR_BW) {
			// either BWR or BWY
			flash_read(address_color,image_in_data_color,4);
			address_color += 4UL;
		}



		in_ptr_black = image_in_data;
		in_ptr_color = image_in_data_color;
		out_ptr = display_cmd_buf;

		for (j=0; j<4; ++j) {
		byte_black =	*(in_ptr_black++);
		byte_color =	*(in_ptr_color++);

		if (display_color == DISPLAY_COLOR_BW) {
			*(out_ptr++) =	(((byte_black&0x80)!=0)?PIXEL_BLACK_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x40)!=0)?PIXEL_BLACK_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x20)!=0)?PIXEL_BLACK_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x10)!=0)?PIXEL_BLACK_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x08)!=0)?PIXEL_BLACK_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x04)!=0)?PIXEL_BLACK_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x02)!=0)?PIXEL_BLACK_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x01)!=0)?PIXEL_BLACK_LOW:PIXEL_WHITE_LOW);

		} else {
			*(out_ptr++) =	(((byte_black&0x80)!=0)?PIXEL_BLACK_HIGH:((byte_color&0x80)!=0)?PIXEL_RED_0_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x40)!=0)?PIXEL_BLACK_LOW:((byte_color&0x40)!=0)?PIXEL_RED_0_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x20)!=0)?PIXEL_BLACK_HIGH:((byte_color&0x20)!=0)?PIXEL_RED_0_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x10)!=0)?PIXEL_BLACK_LOW:((byte_color&0x10)!=0)?PIXEL_RED_0_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x08)!=0)?PIXEL_BLACK_HIGH:((byte_color&0x08)!=0)?PIXEL_RED_0_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x04)!=0)?PIXEL_BLACK_LOW:((byte_color&0x04)!=0)?PIXEL_RED_0_LOW:PIXEL_WHITE_LOW);
			*(out_ptr++) =	(((byte_black&0x02)!=0)?PIXEL_BLACK_HIGH:((byte_color&0x02)!=0)?PIXEL_RED_0_HIGH:PIXEL_WHITE_HIGH) |
							(((byte_black&0x01)!=0)?PIXEL_BLACK_LOW:((byte_color&0x01)!=0)?PIXEL_RED_0_LOW:PIXEL_WHITE_LOW);
		}
		}

		display_spi_transfer(SPI_DATA,device,display_cmd_buf,16,SPI_TRANSFER_NO_CONTINUATION);
		size -= 4;
	}



	display_cmd_buf[0] = UC_CMD_12_DRF;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

	//
	// trigger a read of the battery voltage after 50 ms
	timer_delay(50);
	if (i2c_master_read_register(0x0180,&register_0180) == I2C_M_ACK) {
		if (i2c_master_write_register(0x0180,(register_0180|0x00000400)) == I2C_M_ACK) {
			i2c_master_write_register(0x0180,(register_0180|0x00040000));
		}
	}

	// the mesured render time for my development glass at around 25C was 1.937 seconds
	// wait for 3 seconds total for now
	// FIXME
	timer_delay(DISPLAY_RENDER_INITIAL_WAIT);

	// choose the max based upon tag type...
	// nah, just use a fixed delay now
#if 0	
	k = timer_increment;
	while (DISPLAY_BUSY == 0) {
		// Save some power on the MCU. Let the timer wake it up every quarter second.
		__halt();
		WDTE = 0xAC; // Service the WDT
					
		// maximum 60 second wait for render commands
		if ((timer_increment - k) >= TIMEOUT_RENDER) {
			error |= ERROR_RENDER_TIMEOUT_RENDER;
			goto render_end;
		}
	}
#else
       if(display_wait(TIMEOUT_RENDER)) {
             error |= ERROR_RENDER_TIMEOUT_RENDER;
       }
#endif	

	// disable automatic sampling of the battery voltage
	i2c_master_write_register(0x0180,(register_0180|0x00000400));

	// read the battery voltage
	if (i2c_master_read_register(0x0184,&register_0184) == I2C_M_ACK) {
		//
		register_0184 = (register_0184 >> 16)&0x0FF;
		if (register_0184 < current_voltage_reading) {
			current_voltage_reading = register_0184;
		}
	}

	// read the LPD bit for the display
	display_cmd_buf[0] = UC_CMD_51_LPD;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_CONTINUATION);
	display_spi_read(DISPLAY_SPI_DEVICE_CS1,&lpd_value,1,SPI_TRANSFER_NO_CONTINUATION);

	timer_delay(100);

	// turn off the power
	display_cmd_buf[0] = UC_CMD_02_POF;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);
	//
	// the measured delay time for my development glass was 995 ns at room temp
	i = 10;
	while (DISPLAY_BUSY == 1) {
		if ((i--)==0) {
			error |= ERROR_RENDER_TIMEOUT_POWEROFF;
			goto render_end;
		}

	 	// do nothing but wait
		timer_delay(1);
	}

	timer_delay(100);

#ifdef DISPLAY_DEEP_SLEEP
	// Enable SPI FLASH pass-thru
	display_cmd_buf[0] = UC_CMD_65_DAM;
	display_cmd_buf[1] = 0x01;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
	timer_delay(1);

	// put the display's FLASH chip into deep sleep
	display_cmd_buf[0] = UC_CMD_B9_FLASH_SLEEP;
	display_spi_transfer(SPI_DATA,DISPLAY_SPI_DEVICE_MFCSB,display_cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

	// Disable SPI FLASH pass-thru
	display_cmd_buf[0] = UC_CMD_65_DAM;
	display_cmd_buf[1] = 0x00;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
	timer_delay(1);


	display_cmd_buf[0] = UC_CMD_07_DSLP;
	display_cmd_buf[1] = 0xA5;
	display_spi_transfer(SPI_CMD_DATA,DISPLAY_SPI_DEVICE_CS1,display_cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
#endif	// DISPLAY_DEEP_SLEEP


	// Post status if timeout during wait was necessary
render_end:
	if (error != 0) {
		// timed out waiting for busy
		status = communications_get_register(REGISTER_STATUS);
		if (status == 0xDEADBEEF) {
			status = 0;
		}
		status |= error;
		communications_set_register(REGISTER_STATUS,status);
	}

	// Power down the FLASH
	flash_power(FLASH_POWER_OFF);


	// Physically turn off the display
	timer_delay(10);
	display_enable(DISPLAY_DISABLE);

	/*
#ifdef DISPLAY_DEEP_SLEEP
	DISPLAY_SPI_MOSI = 0;
#else
	DISPLAY_SPI_MOSI = 0;
	DISPLAY_SPI_CLK = 0;
	POWER_DISPLAY_ENA = 1;
	DISPLAY_SPI_CS1 = 0;
	DISPLAY_SPI_CS2 = 0;
	DISPLAY_DC = 0;
	DISPLAY_RESET = 0;
#endif
	*/

	// Borrow "render_count" for render status bits
	render_count = communications_get_register(REGISTER_STATUS) & STATUS_RENDER_STATE_MASK;

	// Use "register_0184" for render fail count (bits 20:13)
	register_0184 = (render_count>>STATUS_RENDER_FAIL_COUNT_SHIFT) & 0xFF;

	// Post status if timeout during wait was necessary
	if (error != 0) {
		// Setup "Render Fail" bits
		render_count |= ( error | STATUS_RENDER_STATE_RENDER_FAIL );

		if( register_0184 < 255 )	// Saturate at 255
		{
			// Bump render fail count for EEPROM write below
			display_cmd_buf[0] = (uint8_t)++register_0184;
		}
	}
	else
	{
		// Setup "Render Pass" bits
		render_count |= STATUS_RENDER_STATE_RENDER_PASS;
	}

	render_count = (render_count&STATUS_RENDER_FAIL_COUNT_MASK) | (register_0184<<STATUS_RENDER_FAIL_COUNT_SHIFT);

	communications_set_register( REGISTER_STATUS, render_count );


	// Update render count
	render_count = communications_get_register(REGISTER_RENDER_COUNT);
	++render_count;
	communications_set_register(REGISTER_RENDER_COUNT,render_count);


	// Update render voltage register
	// reuse register_0180
	register_0180 = communications_get_register(REGISTER_RENDER_VOLTAGE);
	if (lpd_value == 0) {
		// set both bits
		register_0180 |= 0xC0000000;
	} else {
		// clear the current bit, leave the sticky bit alone
		register_0180 &= 0xBFFFFFFF;
	}

	if (error != 0) {
		// set both bits
		register_0180 |= 0x30000000;
	} else {
		// clear the current bit, leave the sticky bit alone
		register_0180 &= 0xEFFFFFFF;
	}

	register_0180 &= 0xFFFFFF00;
	register_0180 |= current_voltage_reading;

	if (((register_0180>>8)&0x0FF) > current_voltage_reading) {
		register_0180 &= 0xFFFF00FF;
		register_0180 |= ((current_voltage_reading<<8)&0x0000FF00);
	}
	
	communications_set_register(REGISTER_RENDER_VOLTAGE,register_0180);

	// this may or may not be safe, commenting out for now...
	//i2c_master_write_register(0x0500,register_0180);
	
	// Update time since last render.
	display_render_complete = 1;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_clear_screen(uint8_t screen) {
	uint32_t startingBlock;

	if (screen >= display_num_screens) {
		return;
	}

	// skip the first block. it might hold provisioning info
	startingBlock = display_blocks_per_screen*screen + 1;

	flash_erase(FLASH_BLOCK_SIZE_4K, startingBlock, display_blocks_per_screen);

}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_spi_transfer(uint8_t mode, uint8_t device, uint8_t *buffer_out, uint16_t size, uint8_t continuation) {
	uint16_t i;
	uint8_t bit;

	if (mode == SPI_CMD_DATA) {
		DISPLAY_DC = 0;
	} else {
		DISPLAY_DC = 1;
	}


	// assert chip select
	if (device & DISPLAY_SPI_DEVICE_CS1) {
		DISPLAY_SPI_CS1 = 0;
	}

	// assert chip select
	if (device & DISPLAY_SPI_DEVICE_CS2) {
		DISPLAY_SPI_CS2 = 0;
	}

	// for the 5.83" tag, this is the MFCSB chip select for the on-flex FLASH chip
	if (device & DISPLAY_SPI_DEVICE_MFCSB) {
		DISPLAY_SPI_MFCSB = 0;
	}

	// perform transfer one byte at a time
	for (i=0; i<size; ++i) {
		for (bit = 0x80; bit != 0; bit >>= 1) {
			if (buffer_out[i] & bit) {
				DISPLAY_SPI_MOSI = 1;
			} else {
				DISPLAY_SPI_MOSI = 0;
			}

			DISPLAY_SPI_CLK = 1;
			DISPLAY_SPI_CLK = 0;
		}

		// force the DC line to data after the first byte regardless of what the first was
		DISPLAY_DC = 1;
	}

	DISPLAY_SPI_MOSI = 0;

	// de-assert chip select
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		DISPLAY_SPI_CS1 = 1;
		DISPLAY_SPI_CS2 = 1;
		DISPLAY_SPI_MFCSB = 1;
	}

	// Service the WDT
	WDTE = 0xAC;
}

void display_spi_transfer_fill(uint8_t device, uint8_t fill, uint32_t count, uint8_t continuation)
{
	uint32_t i;
	uint8_t bit;

	// assert chip select
	if (device & DISPLAY_SPI_DEVICE_CS1) {
		DISPLAY_SPI_CS1 = 0;
	}

	// assert chip select
	if (device & DISPLAY_SPI_DEVICE_CS2) {
		DISPLAY_SPI_CS2 = 0;
	}

	// for the 5.83" tag, this is the MFCSB chip select for the on-flex FLASH chip
	if (device & DISPLAY_SPI_MFCSB) {
		DISPLAY_SPI_MFCSB = 0;
	}

	DISPLAY_SPI_MOSI = 0;

	// perform transfer one byte at a time
	for (i=0; i<count; ++i) {
		for (bit = 0x80; bit != 0; bit >>= 1) {
			if (fill & bit) {
				DISPLAY_SPI_MOSI = 1;
			} else {
				DISPLAY_SPI_MOSI = 0;
			}

			DISPLAY_SPI_CLK = 1;
			DISPLAY_SPI_CLK = 0;
		}
	}

	DISPLAY_SPI_MOSI = 0;

	// de-assert chip select
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		DISPLAY_SPI_CS1 = 1;
		DISPLAY_SPI_CS2 = 1;
		DISPLAY_SPI_MFCSB = 1;
	}

	// Service the WDT
	WDTE = 0xAC;
}

// 
// This routine only reads from a SPI device.	If a normal write/read
//	 pattern is required, display_spi_transfer should be called first
//	 with the SPI_TRANSFER_CONTINUATION flag
// 
// 
//	There are two devices that might be read.
//	First is the UltraChip controller itself.
//		To read from this chip, data is read from the MOSI line.
//		There is no separate MISO line.
//	Second, some EPD include a FLASH chip on the flex.
//		These chips have a separate
// 
void display_spi_read(uint8_t device, uint8_t *buffer_in, uint16_t size, uint8_t continuation) {
	uint16_t i;
	uint8_t bit;
	uint8_t byte_in;

	// assert chip select
	// only one can be asserted for read
	if (device & DISPLAY_SPI_DEVICE_MFCSB) {
		DISPLAY_SPI_MFCSB = 0;
	} else if (device & DISPLAY_SPI_DEVICE_CS1) {
		DISPLAY_SPI_MOSI_DIR = 1;
		DISPLAY_SPI_CS1 = 0;
	} else if (device & DISPLAY_SPI_DEVICE_CS2) {
		DISPLAY_SPI_MOSI_DIR = 1;
		DISPLAY_SPI_CS2 = 0;
	}

	// perform transfer one byte at a time
	for (i=0; i<size; ++i) {
		byte_in = 0x00;

		for (bit = 0x80; bit != 0; bit >>= 1) {
			byte_in<<=1;

			DISPLAY_SPI_CLK = 1;

			if (device & DISPLAY_SPI_DEVICE_MFCSB) {
				byte_in|= DISPLAY_SPI_FMSDO;
			} else {
				byte_in|= DISPLAY_SPI_MOSI;
			}

			DISPLAY_SPI_CLK = 0;
		}

		buffer_in[i] = byte_in;
	}

	// force the display MOSI pin back to an output
	DISPLAY_SPI_MOSI_DIR = 0;

	// de-assert chip select
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		DISPLAY_SPI_CS1 = 1;
		DISPLAY_SPI_CS2 = 1;
		DISPLAY_SPI_MFCSB = 1;
	}

	// Service the WDT
	WDTE = 0xAC;
}


void debug(int16_t count) {
	DEBUG_1 = 1;
	for(int16_t i=0;i<count;i++) {
		DEBUG_0 = 1; DEBUG_0 = 0;
	}
	DEBUG_1 = 0;
}

void display_enable(uint8_t enable) {
	if (enable == DISPLAY_ENABLE) {
		// Set I/O pins to proper state
		DISPLAY_RESET = 1;

		// Power on display
		POWER_DISPLAY_ENA = 0;

		DISPLAY_SPI_CS1 = 1;
		DISPLAY_SPI_CS2 = 1;

		DISPLAY_SPI_SCLK = 0;
//	DISPLAY_DC = 0;
//	timer_delay(300);

		DISPLAY_SPI_MOSI = 1;
		DISPLAY_SPI_MOSI_DIR = 0;

		timer_delay(1);

		// Reset display
		DISPLAY_RESET = 0;
		timer_delay(3);
		DISPLAY_RESET = 1;
		timer_delay(1);

	} else {
#ifdef DISPLAY_DEEP_SLEEP
		DISPLAY_SPI_MOSI = 0;
#else
		DISPLAY_SPI_MOSI = 0;
		DISPLAY_SPI_CLK = 0;
		POWER_DISPLAY_ENA = 1;
		DISPLAY_SPI_CS1 = 0;
		DISPLAY_SPI_CS2 = 0;
		DISPLAY_DC = 0;
		DISPLAY_RESET = 0;
#endif
	}
}


void display_copy_screen(uint32_t cmd) {
	uint32_t address_src;
	uint32_t address_dst;
	uint32_t size_image;
	uint8_t num_bytes;
	uint8_t image_data[DISPLAY_BUFFER_SIZE];

	uint8_t src_screen = cmd >> 16;
	uint8_t dst_screen = cmd >> 8;

	if ((src_screen < display_num_screens) && (dst_screen < display_num_screens)) {
		// Power on the FLASH
		flash_power(FLASH_POWER_ON);

		// clear the screen if the overwrite flag is not set
		if ((cmd&0x00000002) == 0) {
			display_clear_screen(dst_screen);
		}

		if (dual_display_mode == DUAL_DISPLAY) {
			// not currently supported
			//address_src = (uint32_t)src_screen * DISPLAY_SCREEN_SIZE_BW_DUAL;
			//address_dst = (uint32_t)dst_screen * DISPLAY_SCREEN_SIZE_BW_DUAL;
			//size_image = DISPLAY_SCREEN_DATA_SIZE_BW_DUAL;

		} else {
			address_src = (uint32_t)src_screen * display_screen_size_memory;
			address_dst = (uint32_t)dst_screen * display_screen_size_memory;
			size_image = display_screen_size_image;
		}

	
		address_src += DISPLAY_INT_DATA_ADDRESS_OFFSET;
		address_dst += DISPLAY_INT_DATA_ADDRESS_OFFSET;

		while (size_image) {
			num_bytes = DISPLAY_BUFFER_SIZE;
			if (num_bytes < DISPLAY_BUFFER_SIZE) {
				num_bytes = size_image;
			}

			flash_read(address_src,image_data,num_bytes);
			flash_write(address_dst,image_data,num_bytes);

			address_src += num_bytes;
			address_dst += num_bytes;
			size_image -= num_bytes;
		}
	
	}
}




void display_crc(uint32_t cmd) {
	uint8_t screen;
	uint32_t crc;
	uint8_t i;
	uint8_t num_bytes;
	uint32_t address;
	uint32_t size_image;
	uint8_t image_data[DISPLAY_BUFFER_SIZE];


	crc32_init(&crc);

	// Power on the FLASH
	flash_power(FLASH_POWER_ON);

	for (screen = 0; screen < display_num_screens; screen++) {
		if (cmd & (((uint32_t)0x100U) << screen)) {
			
			if (dual_display_mode == DUAL_DISPLAY) {
				// not currently supported
				//address = (uint32_t)screen * DISPLAY_SCREEN_SIZE_BW_DUAL;
				//size_image = DISPLAY_SCREEN_DATA_SIZE_BW_DUAL;
			} else {
				address = (uint32_t)screen * display_screen_size_memory;
				size_image = display_screen_size_image;
			}
		
			address += FLASH_PERSISTENT_MEMORY_OFFSET;
	
			while (size_image) {
				num_bytes = DISPLAY_BUFFER_SIZE;
				if (num_bytes < DISPLAY_BUFFER_SIZE) {
					num_bytes = size_image;
				}


				flash_read(address,image_data,num_bytes);

				for(i=0;i<num_bytes;i++) {
					crc32_compute(&crc, image_data[i]);
				}

				address += num_bytes;
				size_image -= num_bytes;
			}
		
		}
	}

	communications_set_register(REGISTER_CRC_RESULT, crc);
}


void swapBytes (uint8_t *arryPtr, uint8_t len){
  uint8_t tmp;
  uint8_t lenght = len/2;
  len--;
  for(uint8_t i = 0; i < lenght; i++){
    tmp = arryPtr[i];
    arryPtr[i] = arryPtr[len - i];
    arryPtr[len - i] = tmp;
  }
}

