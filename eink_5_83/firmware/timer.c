//----------------------------------------------------------------------------
// timer.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
volatile uint8_t timer_increment = 0;



//----------------------------------------------------------------------------
// Set up the low-frequency 12-bit interval timer.
//----------------------------------------------------------------------------
void timer_initialize(void) {

	// supply IT clock (bit in PER0 register)
	TMKAEN = 1U;

	// disable IT operation
	ITMC = 0x0000U;

	// disable INTIT interrupt
	TMKAMK = 1U;

	// clear INTIT interrupt flag
	TMKAIF = 0U;

	// Set INTIT low priority
	TMKAPR1 = 1U;

	TMKAPR0 = 1U;

	// Base clock rate is 15KHz, this gives an interrupt every 0.256 seconds
	ITMC = 0x0EFFU;

	// enable IT operation
	ITMC |= 0x8000U;

	// clear INTIT interrupt flag
	TMKAMK = 0U;

	// enable INTIT interrupt
	TMKAIF = 0U;
}



//----------------------------------------------------------------------------
// The counter for the interval timer is not exposed to the user, so
// here we use the "timer array unit" instead.
//----------------------------------------------------------------------------
void timer_delay(uint16_t milliseconds) {
	uint16_t previous;
	uint16_t current;

	// Set up timer
	TAU0EN = 1;

	// FCLK = 8MHz, divided by 2^6 = 125KHz
	TPS0 = 0x0006U;

	TMR00 = 0x0000;

	// 1 ms at 125KHz
	TDR00 = 125;

	TS0 = 0x0001;

	while (milliseconds--) {
		// The timer is a repeating countdown timer, so look for transitions from
		// low back to high. This was done to avoid enabling another interrupt.
		previous = 0xFFFF;
		while (1) {
			current = TCR00;
			if (current > previous) {
				break;
			}

			previous = current;
		}
	}

	// Turn off timer
	TT0 = 0x0001;
	TAU0EN = 0;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//
// IAR RL78 Workbench 2.10.1 has a bug in the ISR code that fails to
//	 save scratchpad registers.	2.10.3 has a fix, but that's only for
//	 paying customers.
//	 The workaround is to force the old call format on it with __v1_call
//		 http://documentation.renesas.com/doc/DocumentServer/R20UT3407ED0104_RL78.pdf 
//
#pragma vector = INTIT_vect
__v1_call __interrupt static void timer_isr(void) {

	++timer_increment;
}

//----------------------------------------------------------------------------
// delay for approx. numUS microseconds assuming that we are running at 8MHz MCU clock
// 
//----------------------------------------------------------------------------
void busyWait(uint8_t numUS) {
	for (uint8_t i= 0; i <= numUS; i++) {
		__no_operation();
		__no_operation();
		__no_operation();
		__no_operation();
		__no_operation();
		__no_operation();
	}
}
