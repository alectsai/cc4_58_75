//----------------------------------------------------------------------------
// flash.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define FLASH_POWER_OFF 0
#define FLASH_POWER_ON  1

#define FLASH_POWER_TIMEOUT_COUNT 10 // in increments of 1/4 seconds

#define FLASH_BLOCK_SIZE_256		(0x00100)
#define FLASH_BLOCK_SIZE_4K			(0x01000)
#define FLASH_BLOCK_SIZE_32K		(0x08000)

#define FLASH_DEVICE_SIZE_256_KBIT	(32L*1024L)
#define FLASH_DEVICE_SIZE_512_KBIT	(64L*1024L)
#define FLASH_DEVICE_SIZE_1_MBIT	(128L*1024L)
#define FLASH_DEVICE_SIZE_2_MBIT	(256L*1024L)
#define FLASH_DEVICE_SIZE_4_MBIT	(512L*1024L)
#define FLASH_DEVICE_SIZE_8_MBIT	(1024L*1024L)

// Flash commands
#define FLASH_CMD_RD_ID			((uint8_t) 0x9F)		// read manufacturer device ID
#define FLASH_CMD_WR_EN			((uint8_t) 0x06)		// write enable
#define FLASH_CMD_WR_DIS		((uint8_t) 0x04)		// write disable
#define FLASH_CMD_WR			((uint8_t) 0x02)		// write data
#define FLASH_CMD_RD			((uint8_t) 0x03)		// read data
#define FLASH_CMD_WR_STATUS		((uint8_t) 0x01)		// write status
#define FLASH_CMD_RD_STATUS		((uint8_t) 0x05)		// read status
#define FLASH_CMD_PWR_DN		((uint8_t) 0x79)		// ultra deep power down
#define FLASH_CMD_PWR_UP		((uint8_t) 0xAB)		// resume from deep power down
#define FLASH_CMD_ERASE_PAGE	((uint8_t) 0x81)		// erase a 256 byte page
#define FLASH_CMD_ERASE_BLK_4K	((uint8_t) 0x20)		// erase a 4K byte block
#define FLASH_CMD_ERASE_BLK_32K	((uint8_t) 0x52)		// erase a 32K byte block

#define FLASH_ID_ADESTO				((uint8_t) 0x1F)	// Adesto manufacturer ID
#define FLASH_ID_ADESTO_256_KBIT	((uint8_t) 0x40)	// Adesto 256kbit flash device ID
#define FLASH_ID_ADESTO_512_KBIT	((uint8_t) 0x65)	// Adesto 512kbit flash device ID
#define FLASH_ID_ADESTO_1_MBIT		((uint8_t) 0x42)	// Adesto 1Mbit flash device ID
#define FLASH_ID_ADESTO_2_MBIT		((uint8_t) 0x43)	// Adesto 2MBit device ID
#define FLASH_ID_ADESTO_4_MBIT		((uint8_t) 0x44)	// Adesto 4MB device ID


extern uint8_t flash_power_timeout;



//----------------------------------------------------------------------------
// The first 4 KB of FLASH are reserved for "persistent" memory.
// This memory is configuration data that is maintained through a power
// cycle.
//
// Starting at address 0x00000000 (addressed externallay as 0xF000) is
// space allocated to configuration and provisioning
// the command/waveform tables for the ultrachip controller.
// Persistent state variables are allocated from the top of the persistent
// memory area.
//----------------------------------------------------------------------------
#define FLASH_PERSISTENT_MEMORY_OFFSET 0x00000000



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern uint32_t flash_memory_size;
extern uint16_t flash_page_size;



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void flash_initialize(void);
void flash_power(uint8_t state);
void flash_write(uint32_t address, uint8_t *buffer, uint16_t size);
void flash_read(uint32_t address, uint8_t *buffer, uint16_t size);
void flash_page_erase(uint32_t pageAdr);
void flash_erase(uint32_t blockSize, uint32_t block, uint32_t count);
void erase_prov_area();
