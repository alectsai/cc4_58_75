//------------------------------------------------------------
// error.c - functions that are used for reporting errors for debugging
//---------------------------------------------------------------
#include "all_includes.h"

void error_report(uint32_t errNum) {
	uint32_t status;
	// get status from CC4
	status = communications_get_register(REGISTER_STATUS);

	// timed out waiting for busy
	status = communications_get_register(REGISTER_STATUS);
	if (status == 0xDEADBEEF) {
		status = 0;
	}

	status |= errNum;
	communications_set_register(REGISTER_STATUS,status);
}
