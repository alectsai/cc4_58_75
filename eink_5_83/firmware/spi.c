//----------------------------------------------------------------------------
// spi.c
//----------------------------------------------------------------------------
#include "all_includes.h"


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void spi_initialize(void) {

	// Turn on the SAU0 clock
	SAU0EN = 1;
	timer_delay(1);

	// Set the SPI ck00 and ck01 clock to MAX (fclk)
	SPS0 = 0x0000;

	// Set device chip selects (display selects, handled in display.c)
	// Initial pin settings are configured in event_manager.c

	// Initialize channel 0
	//
	// disable channel 0
	ST0 = 0x0001U;
	//
	// disable INTCSI00 interrupt
	CSIMK00 = 1U;
	//
	// clear INTCSI00 interrupt flag
	CSIIF00 = 0U;
	//
	// clear the error flags
	SIR00 = 0x07U;

	// Serial Mode Register
	//		CKS00 = 0:	0x8000
	//		CCS00 = 0:	0x4000
	//		n/a	 = 0:	0x0020
	//		MD002 = 0:	0x0004 (4: IIC)
	//		MD001 = 0:	0x0002 (2: UART)
	//		MD000 = 0:	0x0001 (0: CSI)
	SMR00 = 0x0020U;

	// Serial communication operation setting register
	//		TXE00 =	1: 0x8000	(TX allowed on this port)
	//		RXE00 =	1: 0x4000	(RX allowed on this port)
	//		DAP00 =	1: 0x2000	(Type 4 timing)
	//		CKP00 =	1: 0x1000
	//		n/a	 =	0: 0x0800
	//		EOC00 =	0: 0x0400
	//		PTC001 = 0: 0x0200
	//		PTC000 = 0: 0x0100
	//		DIR00 =	0: 0x0080
	//		n/a	 =	0: 0x0040
	//		SLC001 = 0: 0x0020
	//		SLC000 = 0: 0x0010
	//		n/a	 =	0: 0x0008
	//		n/a	 =	1: 0x0004
	//		CLS001 = 1: 0x0002	(data length: 8 bits)
	//		CLS000 = 1: 0x0001
	SCR00 = 0xF007U;

	// 4 MHz (1/4) for 16 MHz operation >= 2.4 V
	SDR00 = 0x0200U;

	// Set CSI00 initial levels: CK: 0 SO: 0
	SO0 &= 0xFEFEU;

	// enable Serial output 
	SOE0 = 1;

	// start the SPI interface
	SS0 = 1;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void spi_power(uint8_t state) {

	if (state == SPI_POWER_ON) {
		SPI_MEMORY_CS = 1;

		// stop CSI00
		ST0 |= 0x0001U;

		// restore the SPI signal levels
		SPI_SCLK = 1;
		SPI_MOSI = 1;

		// enable CSI00 output
		SOE0 |= 0x0001U;
		SCR00 |= 0x4000U;
		SS0 |= 0x0001U;

	} else {
		// stop CSI00
		ST0 |= 0x0001U;

		// disable CSI00 output
		SOE0 &= 0xFFFEU;

		SPI_SCLK = 0;
		SPI_MOSI = 0;

		SPI_MEMORY_CS = 0;
	}
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void spi_transfer(uint8_t device, uint8_t *buffer_out, uint8_t *buffer_in, uint16_t size, uint8_t continuation, uint8_t invertData) {
	uint16_t i;
	uint8_t dummy;

	// assert chip select
	SPI_MEMORY_CS = 0;

	// perform transfer once byte at a time
	for (i=0; i<size; ++i) {

		if (invertData == 1) {
			// invert the data because we use flash
			// flash have a 0xFF erase state
			SIO00 = ~buffer_out[i];
																		 
		} else {
			// send a byte
			SIO00 = buffer_out[i];
		}

		// Ported from PDI code. Don't ask me why but
		// if you don't do this then the value you read
		// back after the transfer is just the value you sent.
		__no_operation();
		__no_operation();
		__no_operation();
		__no_operation();

		// wait for it to be finished
		while ((SSR00 & 0x0040U) != 0);

		if (buffer_in == NULL) {
			// dump the received byte
			dummy = SIO00;

			// to remove compiler warning
			dummy = dummy;

		} else {
			if (invertData == 1) {
				// invert data because we use flash
				buffer_in[i] = ~SIO00;

			} else {
		 		// read the received byte
				buffer_in[i] = SIO00;
			}
		}
	}

	// de-assert chip select
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		SPI_MEMORY_CS = 1;

	}

	// Service the WDT
	WDTE = 0xAC;
}
