//----------------------------------------------------------------------------
// i2c_master.h
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define I2C_M_SCL     (P0_bit.no1)
#define I2C_M_SDA     (P4_bit.no1)
#define I2C_M_DIR_SCL (PM0_bit.no1)
#define I2C_M_DIR_SDA (PM4_bit.no1)

#define I2C_M_CC_ID    (0x41)
#define I2C_M_CC_WRITE (I2C_M_CC_ID << 1)
#define I2C_M_CC_READ  ((I2C_M_CC_ID << 1) | 1)

#define I2C_M_ACK  1
#define I2C_M_NACK 0

#define I2C_M_CLK_WAIT_TIMEOUT 64



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern void i2c_master_initialize(void);
extern uint8_t i2c_master_write_register(uint32_t address, uint32_t value);
extern uint8_t i2c_master_read_register(uint32_t address, uint32_t *value);
extern uint8_t i2c_master_write_byte(uint8_t value);
extern uint8_t i2c_master_read_byte(uint8_t end, uint8_t *value);
extern void i2c_master_delay(void);
extern void indirect_i2c_write(uint32_t address, uint32_t value);
